<?php

/**
 * Class Aries_Polymer_Shortcodes
 */
class Aries_Polymer_Shortcodes {
    /**
     * Aries_Polymer_Shortcodes constructor.
     */
    public function __construct() {
        if(get_theme_mod( 'ariespolymer_default_gallery' ) != 'wordpress'){
            remove_shortcode('gallery');
            add_shortcode('gallery', array($this, 'parse_gallery_shortcode'));
        }
        add_shortcode('ap-contact-form', array($this, 'polymer_contact_form'));
    }

    /**
     * @param $atts
     * @return string
     */
    public function parse_gallery_shortcode($atts) {

        global $post;

        if(!empty($atts['ids'])) {
            // 'ids' is explicitly ordered, unless you specify otherwise.
            if(empty($atts['orderby'])) {
                $atts['orderby'] = 'post__in';
            }
            $atts['include'] = $atts['ids'];
        }

        extract(shortcode_atts(array('orderby' => 'menu_order ASC, ID ASC', 'include' => '', 'id' => $post->ID), $atts));


        $args = array('post_type' => 'attachment', 'post_status' => 'inherit', 'post_mime_type' => 'image', 'orderby' => $orderby);

        if(!empty($include)) {
            $args['include'] = $include;
        } else {
            $args['post_parent'] = $id;
            $args['numberposts'] = -1;
        }

        $images = get_posts($args);

        $out = '<sc-swiper pagination pagination-clickable loop navigation-buttons >';

        foreach($images as $image) {
            $caption = $image->post_excerpt;

            $description = $image->post_content;
            if($description == '') {
                $description = $image->post_title;
            }

            $image_alt = get_post_meta($image->ID, '_wp_attachment_image_alt', true);
            $image_url =  wp_get_attachment_url($image->ID);

            $out .= '<img  src="'.$image_url.'" alt="'.$image_alt.'"/>';
        }
        $out .= '</sc-swiper>';

        return $out;
    }

    /**
     * @param $atts
     * @return string
     */
    public function polymer_contact_form($atts){

        extract(shortcode_atts(array('recapkey' => "", 'recapsecret' => ""), $atts));
        $error = $emailError = $nameError = $subjectError = $contentError = $sent = $recaptchaerror = false;
        //response messages
        $missing_name = __( "This is a required field.", 'ariespolymer' );
        $missing_subject = __( "This is a required field.", 'ariespolymer' );
        $missing_content = __( "This is a required field.", 'ariespolymer' );
        $email_invalid   = __( "Email Address Invalid.", 'ariespolymer' );

        //user posted variables
        $name = (isset($_POST['message_name'])) ? $_POST['message_name'] : null;
        $email = (isset($_POST['message_name'])) ? $_POST['message_email'] : null;
        $subject = (isset($_POST['message_name'])) ? $_POST['message_subject'] : null;
        $message = (isset($_POST['message_name'])) ? $_POST['message_text'] : null;
        $submit = (isset($_POST['message_name'])) ? $_POST['contact-form-submit'] : null;
        $successMessage = __( 'Your message has been successfully sent.', 'ariespolymer' );
        $sendMessageError = __( 'There was an error trying to send your message. Please try again later.', 'ariespolymer' );

        if(!empty($submit)) {
            //validate email
            if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {$error = true; $emailError = true;}
            if(empty($name) || $name == ""){$error = true; $nameError = true;}
            if(empty($subject) || $subject == ""){$error = true; $subjectError = true;}
            if(empty($message) || $message == ""){$error = true; $contentError = true;}
            if(isset($_POST['g-recaptcha-response']) && $recapsecret != "" && $recapkey != "") {
                $captcha=$_POST['g-recaptcha-response'];
                $remote_ip = $_SERVER["REMOTE_ADDR"];

                $request = wp_remote_get(
                    'https://www.google.com/recaptcha/api/siteverify?secret='.$recapsecret.'&response=' . $captcha . '&remoteip=' . $remote_ip
                );
                $response_body = wp_remote_retrieve_body( $request );
                $result = json_decode( $response_body, true );

                if(!$result['success']){
                    $error = $recaptchaerror = true;
                    $sendMessageError = __( 'You have entered an invalid value for the captcha.', 'ariespolymer' );
                }
            }



            if(!$error){
                //php mailer variables
                $urlParts = parse_url(site_url());
                $to = get_option('admin_email');
                $headers = 'From: '. 'no-reply@'.$urlParts['host'] . "\r\n" . 'Reply-To: ' . $email . "\r\n";
                $messageTop =  __( 'From: ', 'ariespolymer' ).$name." ".$email."\r\n";
                $messageTop .=  __( 'Subject: ', 'ariespolymer' ). $subject;
                $message = $messageTop."\r\n"."\r\n".strip_tags($message);
                $sent = wp_mail($to, $subject, $message, $headers);
            }
        }
        ob_start();
    ?>
        <?php if($recapsecret != "" && $recapkey != ""): ?>
        <script src="https://www.google.com/recaptcha/api.js" async defer></script>
        <?php endif ?>
        <paper-material id="contact-form-wrapper" class="container-class" elevation="0">
            <?php if(!empty($submit) && !$error && $sent):?>
                <paper-toast id="toast-success" text="<?php echo $successMessage ?>" opened  class="aries-polymer-success fit-bottom">
                </paper-toast>
            <?php endif ?>
            <?php if((!empty($submit) &&  !$error  && !$sent) || (!empty($submit) && $recaptchaerror)):?>
                <paper-toast id="toast-error" text="<?php echo $sendMessageError ?>" opened  class="aries-polymer-error fit-bottom">
                </paper-toast>
            <?php endif ?>
            <form is="iron-form" id="contact-form" method="post" action="<?php the_permalink(); ?>">
                <paper-input type="text" label="<?php  echo __( 'Name', 'ariespolymer' )?> *" name="message_name" value="<?php echo ($error)?$name:""?>" <?php if($nameError)echo 'invalid '?>error-message="<?php echo $missing_name?>"></paper-input>
                <paper-input label="<?php  echo __( 'Email', 'ariespolymer' ) ?> *" name="message_email" value="<?php echo ($error)?$email:""?>"  <?php if($emailError)echo 'invalid '?>error-message="<?php echo $email_invalid?>"></paper-input></p>
                <paper-input label="<?php  echo __( 'Subject', 'ariespolymer' ) ?> *" name="message_subject" value="<?php echo ($error)?$subject:""?>"  <?php if($subjectError)echo 'invalid '?>error-message="<?php echo $missing_subject?>"></paper-input></p>
                <paper-textarea rows="5" label="<?php  echo  __( 'Message', 'ariespolymer' ) ?> *" name="message_text" value="<?php echo ($error)?$message:""?>"  <?php if($contentError)echo 'invalid '?>error-message="<?php echo $missing_content?>"></paper-textarea>
                <?php if($recapsecret != "" && $recapkey != ""): ?>
                <div class="recaptcha" style=";position:relative;margin:20px 0;">
                    <div class="g-recaptcha" data-sitekey="<?php echo $recapkey ?>"></div>
                </div>
                <?php endif ?>
                <button class="paper-button-wrapper" type="submit" name="contact-form-submit" value="contact-form-submit">
                    <paper-button raised name="contact-form-submit" id="contact-form-submit" class="contact-form-submit" >
                        <?php echo __('Send', 'ariespolymer' ) ?>
                    </paper-button>
                </button>
            </form>
        </paper-material>
    <?php
        return ob_get_clean();
    }

}
new Aries_Polymer_Shortcodes();