<?php


/**
 * Class Aries_Polymer_Nav_Menu_Icons
 */
class Aries_Polymer_Nav_Menu_Icons {

	/**
	* Nav_Menu_Icons Constructor.
	* @access public
	*/
	public function __construct(){

        add_filter( 'wp_edit_nav_menu_walker', array( $this, 'edit_nav_menu_walker' ), 99 );

		add_action( 'wp_nav_menu_item_custom_fields', array( $this, 'custom_fields' ), 10, 4 );

		add_action( 'wp_update_nav_menu_item', array( $this, 'nav_update'), 10, 2 );

        add_action( 'admin_head', array( $this, 'includeFiles') );

        add_action('admin_footer',  array( $this, 'includeModal') );


	}


    /**
     *
     */
    public function includeFiles(){
	    if(get_current_screen()->in_admin() && get_current_screen()->id == "nav-menus"){
        ?>
            <!-- build:js bower_components/webcomponentsjs/webcomponents-lite.min.js -->
            <script src="<?php echo get_template_directory_uri(); ?>/bower_components/webcomponentsjs/webcomponents-lite.js"></script>
            <!-- endbuild -->

            <!-- will be replaced with elements/elements.vulcanized.html -->
            <link rel="import" href="<?php echo get_template_directory_uri(); ?>/elements/admin-elements.html">

            <script>

                var currentMenuItem = "";

                var onclickIcon = function(icon){
                    document.getElementById('actions').close();
                    if(currentMenuItem != ""){
                        document.getElementById('nav-menu-icon-'+ currentMenuItem).value = icon;
                        var myInput = document.getElementById('iron-icon-'+ currentMenuItem);
                        myInput.setAttribute('icon', icon);
                    }
                    return false;
                }
                var openActions = function(menu_id){
                    currentMenuItem = menu_id
                    document.getElementById('actions').open();
                }
                var removeIcon = function(menu_id){
                    document.getElementById('nav-menu-icon-'+ menu_id).value = "";
                    var myInput = document.getElementById('iron-icon-'+ menu_id);
                    myInput.setAttribute('icon', "");
                }
            </script>
            <style>
                #adminmenuwrap{
                    z-index: 9990;
                }
                .admin-polymer-icon-dialog{
                    z-index: 9991;
                }
                .iron-icon-anchor{
                    color: #666666;
                    display: inline-block;
                }
                .iron-icon-anchor:hover{
                    color: #444444;
                }
            </style>

        <?php
        }
    }

    /**
     *
     */
    public function includeModal(){
        if(get_current_screen()->in_admin() && get_current_screen()->id == "nav-menus"){

            $icons = $this->getIconsArray();
            ?>
            <paper-dialog id="actions" class="admin-polymer-icon-dialog">
                <h2><?php echo __( 'Polymer Icons', 'ariespolymer' ) ?></h2>
                <?php foreach($icons as $val): ?>
                    <a href="#" onclick="onclickIcon('<?php echo $val ?>')" class="iron-icon-anchor"><iron-icon icon="<?php echo $val ?>"></iron-icon></a>
                <?php endforeach; ?>
                <div class="buttons">
                    <paper-button dialog-dismiss><?php echo __( 'Close', 'ariespolymer' ) ?></paper-button>
                </div>
            </paper-dialog>

            <?php
        }
    }

	/**
	* Override the Admin Menu Walker
	* @since 1.0
	*/
	public function edit_nav_menu_walker( $walker ) {
        if ( ! class_exists( 'Aries_Polimer_Walker_Nav_Menu_Edit' ) ) {
            require_once(get_template_directory()."/classes/Aries_Polimer_Walker_Nav_Menu_Edit.php");
        }
        $walker = 'Aries_Polimer_Walker_Nav_Menu_Edit';

        return $walker;
	}


	/**
	* @params obj $item - the menu item
	* @params array $args
	*/
	public function custom_fields( $item_id, $item, $depth, $args ) {

		$icon = get_post_meta( $item->ID, '_nav_menu_icon', true );
		?>

        <p class="description description-wide">
            <label for="nav-menu-icon"><?php echo __( 'Polymer Icon', 'ariespolymer' ) ?>: </label>
            <iron-icon id="iron-icon-<?php echo $item->ID?>" icon="<?php echo $icon ?>"></iron-icon>
            <input type="hidden" id="nav-menu-icon-<?php echo $item->ID ?>" name="nav-menu-icon[<?php echo $item->ID ?>]" class="widefat" value="<?php echo $icon ?>"/>
        </p>
        <paper-button raised onclick="openActions('<?php echo $item->ID ?>')"><?php echo __( 'Set/Change Icon', 'ariespolymer' ) ?></paper-button>
        <paper-button raised onclick="removeIcon('<?php echo $item->ID ?>')"><?php echo __( 'Remove Icon', 'ariespolymer' ) ?></paper-button>

		<?php
	}


	/**
	* @return string
	*/
	public function nav_update( $menu_id, $menu_item_db_id ) {

        if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
            return;
        }

        $screen = get_current_screen();
        if ( ! $screen instanceof WP_Screen || 'nav-menus' !== $screen->id ) {
            return;
        }

        check_admin_referer( 'update-nav_menu', 'update-nav-menu-nonce' );

		if ( isset( $_POST['nav-menu-icon'][$menu_item_db_id]  )  && ! empty ( $_POST['nav-menu-icon'][$menu_item_db_id] ) ) {
            update_post_meta( $menu_item_db_id, '_nav_menu_icon', $_POST['nav-menu-icon'][$menu_item_db_id] );
		}else{
            delete_post_meta( $menu_item_db_id, '_nav_menu_icon' );
        }

	}

    /**
     * @return array
     */
    public function getIconsArray(){
	    $icons = array("3d-rotation","accessibility","account-balance","account-balance-wallet","account-box","account-circle","add","add-alert","add-box","add-circle","add-circle-outline",
                       "add-shopping-cart","alarm","alarm-add","alarm-off","alarm-on","android","announcement","apps","archive","arrow-back",
                       "arrow-drop-down","arrow-drop-down-circle","arrow-drop-up","arrow-forward","aspect-ratio","assessment","assignment","assignment-ind","assignment-late","assignment-return",
                       "assignment-returned","assignment-turned-in","attachment","autorenew","backspace","backup","block","book","bookmark","bookmark-border",
                       "bug-report","build","cached","camera-enhance","cancel","card-giftcard","card-membership","card-travel","change-history","check",
                       "check-box","check-box-outline-blank","check-circle","chevron-left","chevron-right","chrome-reader-mode","class","clear","close","cloud",
                       "cloud-circle","cloud-done","cloud-download","cloud-off","cloud-queue","cloud-upload","code","content-copy","content-cut","content-paste",
                       "create","credit-card","dashboard","delete","description","dns","done","done-all","drafts","eject",
                       "error","error-outline","event","event-seat","exit-to-app","expand-less","expand-more","explore","extension","face",
                       "favorite","favorite-border","feedback","file-download","file-upload","filter-list","find-in-page","find-replace","flag","flight-land",
                       "flight-takeoff","flip-to-back","flip-to-front","folder","folder-open","folder-shared","font-download","forward","fullscreen","fullscreen-exit",
                       "gesture","get-app","gif","grade","group-work","help","help-outline","highlight-off","history","home",
                       "hourglass-empty","hourglass-full","http","https","inbox","indeterminate-check-box","info","info-outline","input","invert-colors",
                       "label","label-outline","language","launch","link","list","lock","lock-open","lock-outline","loyalty",
                       "mail","markunread","markunread-mailbox","menu","more-horiz","more-vert","note-add","offline-pin","open-in-browser","open-in-new",
                       "open-with","pageview","payment","perm-camera-mic","perm-contact-calendar","perm-data-setting","perm-device-information","perm-identity","perm-media","perm-phone-msg",
                       "perm-scan-wifi","picture-in-picture","play-for-work","polymer","power-settings-new","print","query-builder","question-answer","radio-button-checked","radio-button-unchecked",
                       "receipt","redeem","redo","refresh","remove","remove-circle","remove-circle-outline","reorder","reply","reply-all",
                       "report","report-problem","restore","room","save","schedule","search","select-all","send","settings",
                       "settings-applications","settings-backup-restore","settings-bluetooth","settings-brightness","settings-cell","settings-ethernet","settings-input-antenna","settings-input-component","settings-input-composite","settings-input-hdmi",
                       "settings-input-svideo","settings-overscan","settings-phone","settings-power","settings-remote","settings-voice","shop","shop-two","shopping-basket","shopping-cart",
                       "sort","speaker-notes","spellcheck","star","star-border","star-half","stars","store","subject","supervisor-account",
                       "swap-horiz","swap-vert","swap-vertical-circle","system-update-alt","tab","tab-unselected","text-format","theaters","thumb-down","thumb-up",
                       "thumbs-up-down","toc","today","toll","track-changes","translate","trending-down","trending-flat","trending-up","turned-in",
                       "turned-in-not","undo","unfold-less","unfold-more","verified-user","view-agenda","view-array","view-carousel","view-column","view-day",
                       "view-headline","view-list","view-module","view-quilt","view-stream","view-week","visibility","visibility-off","warning","work",
                       "youtube-searched-for","zoom-in","zoom-out");
	    return $icons;
    }

    /**
     *
     */
    private function filterIconId(){
        $xml = '<data><g id="3d-rotation"><path d="81 1.33-1.33c3.27 1.55 5.61 4.72 5.96 8.48h1.5C23.44 4.84 18.29 0 12 0z"/></g></data>';
        $doc = new DOMDocument();

        $doc->loadXML($xml);


        $searchNode = $doc->getElementsByTagName( "g" );

        $count = 0;

        foreach( $searchNode as $searchNode )
        {
            $valueID = $searchNode->getAttribute('id');
            $out = "\"$valueID\",";
            if($count == 10){
                $out .= "\n";
                $count = 0;
            }
            echo $out;
            $count++;
        }
    }


} // end class


new Aries_Polymer_Nav_Menu_Icons();