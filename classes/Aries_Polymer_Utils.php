<?php


class Aries_Polymer_Utils {

    public static function getPaginationNav(){
        global $wp_query;
        $total = $wp_query->max_num_pages;

        if ( !$current_page = get_query_var('paged') )
            $current_page = 1;

        if($total > 1){
            ?>
            <paper-material elevation="0" class="aries-polymer-nav layout horizontal center center-justified style-scope">
                <?php if($current_page != $total):?>
                <paper-button raised class="secondary"><?php next_posts_link(__('Previous Posts', 'ariespolymer')) ?></paper-button>
                <?php endif ?>
                <?php if($current_page != 1):?>
                <paper-button raised class="secondary"><?php previous_posts_link(__('Next posts', 'ariespolymer')) ?></paper-button>
                <?php endif ?>
            </paper-material>
            <?php
        }

    }

    public static function ariespolymer_time_link() {
        $time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
        if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
            $time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
        }

        $time_string = sprintf( $time_string,
                                get_the_date( DATE_W3C ),
                                get_the_date(),
                                get_the_modified_date( DATE_W3C ),
                                get_the_modified_date()
        );

        // Wrap the time string in a link, and preface it with 'Posted on'.
        return sprintf(
        /* translators: %s: post date */
            __( '<span class="screen-reader-text">Posted on</span> %s', 'ariespolymer' ),
            '<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
        );
    }

    public static function getCommentFormArgs(){
        $fields = array('author' => '<p class="comment-form-author"><paper-input label="' . __( 'Name', 'ariespolymer' ) . ' *" name="author" required></paper-input></p>',
                        'email'  => '<p class="comment-form-email"><paper-input label="' . __( 'Email', 'ariespolymer' ) . ' *" name="email" required></paper-input></p>',
                        'url'    => '<p class="comment-form-url"><paper-input label="' . __( 'Website', 'ariespolymer' ) . ' *" name="url" required></paper-input></p>');

        $fields = apply_filters( 'comment_form_default_fields', $fields );
        $args = array(
            'fields'               => $fields,
            'comment_field'        => '<p class="comment-form-comment"><paper-textarea rows="5" label="' . __( 'Comment', 'ariespolymer' ) . ' *" name="comment" required></paper-textarea></p>',
            'title_reply'          => __( 'Leave a Reply' ),
            'title_reply_to'       => __( 'Leave a Reply to %s' ),
            'title_reply_before'   => '<h3 id="reply-title" class="comment-reply-title">',
            'title_reply_after'    => '</h3>',
            'cancel_reply_before'  => ' <small>',
            'cancel_reply_after'   => '</small>',
            'cancel_reply_link'    => __( 'Cancel reply' ),
            'label_submit'         => __( 'Post Comment' ),
            'submit_button'        => '<paper-button raised onclick="document.getElementById(\'commentform\').submit()" name="%1$s" id="%2$s" class="%3$s" >%4$s</paper-button>',
            'submit_field'         => '<p class="form-submit">%1$s %2$s</p>',
            'format'               => 'html',
        );
        return $args;
    }

    public static function  getSocialMediaHeader(){
        $color = self::getAriesPolymerThemeMod('ariespolymer_header_color', '#ffffff');
    ?>
        <div class="layout horizontal center center-justified style-scope pw-footer header-social-media-icons">
            <?php if( $themeOptions = self::getAriesPolymerThemeMod('ariespolymer_social_facebook') ):?>
                <a href="<?php echo $themeOptions ?>" target="_blank">
                    <social-media-icons icon="facebook" color="<?php echo $color ?>" size="24"></social-media-icons>
                </a>
            <?php endif ?>
            <?php if( $themeOptions = self::getAriesPolymerThemeMod('ariespolymer_social_twitter') ):?>
                <a href="<?php echo $themeOptions ?>"  target="_blank">
                    <social-media-icons icon="twitter" color="<?php echo $color ?>" size="24"></social-media-icons>
                </a>
            <?php endif ?>
            <?php if( $themeOptions =  self::getAriesPolymerThemeMod('ariespolymer_social_google') ):?>
                <a href="<?php echo $themeOptions ?>"  target="_blank">
                    <social-media-icons icon="googleplus" color="<?php echo $color?>" size="24"></social-media-icons>
                </a>
            <?php endif ?>
            <?php if( $themeOptions = self::getAriesPolymerThemeMod('ariespolymer_social_instagram')  ):?>
                <a href="<?php echo $themeOptions ?>"  target="_blank">
                    <social-media-icons icon="instagram" color="<?php echo $color ?>" size="24"></social-media-icons>
                </a>
            <?php endif ?>
            <?php if( $themeOptions = self::getAriesPolymerThemeMod('ariespolymer_social_linkedin')  ):?>
                <a href="<?php echo $themeOptions?>"  target="_blank">
                    <social-media-icons icon="linkedin" color="<?php echo $color ?>" size="24"></social-media-icons>
                </a>
            <?php endif ?>
            <?php if( $themeOptions =  self::getAriesPolymerThemeMod('ariespolymer_social_pinterest') ):?>
                <a href="<?php echo $themeOptions ?>"  target="_blank">
                    <social-media-icons icon="pinterest" color="<?php echo $color ?>" size="24"></social-media-icons>
                </a>
            <?php endif ?>
            <?php if( $themeOptions =  self::getAriesPolymerThemeMod('ariespolymer_social_tumblr')):?>
                <a href="<?php echo $themeOptions ?>"  target="_blank">
                    <social-media-icons icon="tumblr" color="<?php echo $color ?>" size="24"></social-media-icons>
                </a>
            <?php endif ?>
            <?php if( $themeOptions = self::getAriesPolymerThemeMod('ariespolymer_social_youtube') ):?>
                <a href="<?php echo $themeOptions  ?>"  target="_blank">
                    <social-media-icons icon="youtube" color="<?php echo $color ?>" size="24"></social-media-icons>
                </a>
            <?php endif ?>
            <?php if( $themeOptions = self::getAriesPolymerThemeMod('ariespolymer_social_vimeo') ):?>
                <a href="<?php echo $themeOptions ?>"  target="_blank">
                    <social-media-icons icon="vimeo" color="<?php echo $color ?>" size="24"></social-media-icons>
                </a>
            <?php endif ?>
        </div>
    <?php
    }

    public static function  getSocialMediaFooter(){
        ?>
        <div class="layout horizontal center center-justified style-scope pw-footer">
            <?php if( $themeOptions = get_theme_mod( 'ariespolymer_social_facebook' ) ):?>
                <a href="<?php echo $themeOptions ?>" target="_blank"><social-media-icons icon="facebook"></social-media-icons></a>
            <?php endif ?>
            <?php if( $themeOptions = get_theme_mod( 'ariespolymer_social_twitter' ) ):?>
                <a href="<?php echo $themeOptions ?>"  target="_blank"><social-media-icons icon="twitter"></social-media-icons></a>
            <?php endif ?>
            <?php if( $themeOptions = get_theme_mod( 'ariespolymer_social_google' ) ):?>
                <a href="<?php echo $themeOptions ?>"  target="_blank"><social-media-icons icon="googleplus"></social-media-icons></a>
            <?php endif ?>
            <?php if( $themeOptions = get_theme_mod( 'ariespolymer_social_instagram' ) ):?>
                <a href="<?php echo $themeOptions ?>"  target="_blank"><social-media-icons icon="instagram"></social-media-icons></a>
            <?php endif ?>
            <?php if( $themeOptions = get_theme_mod( 'ariespolymer_social_linkedin' ) ):?>
                <a href="<?php echo $themeOptions ?>"  target="_blank"><social-media-icons icon="linkedin"></social-media-icons></a>
            <?php endif ?>
            <?php if( $themeOptions = get_theme_mod( 'ariespolymer_social_pinterest' ) ):?>
                <a href="<?php echo $themeOptions ?>"  target="_blank"><social-media-icons icon="pinterest"></social-media-icons></a>
            <?php endif ?>
            <?php if( $themeOptions = get_theme_mod( 'ariespolymer_social_tumblr' ) ):?>
                <a href="<?php echo $themeOptions ?>"  target="_blank"><social-media-icons icon="tumblr"></social-media-icons></a>
            <?php endif ?>
            <?php if( $themeOptions = get_theme_mod( 'ariespolymer_social_youtube' ) ):?>
                <a href="<?php echo $themeOptions ?>"  target="_blank"><social-media-icons icon="youtube"></social-media-icons></a>
            <?php endif ?>
            <?php if( $themeOptions = get_theme_mod( 'ariespolymer_social_vimeo' ) ):?>
                <a href="<?php echo $themeOptions ?>"  target="_blank"><social-media-icons icon="vimeo"></social-media-icons></a>
            <?php endif ?>
        </div>
        <?php
    }

    public static function getCustomizeStyleSheets(){
        $fonts = Aries_Polymer_Setup::get_my_available_fonts();
    ?>
        <style is="custom-style">

            paper-scroll-header-panel {
                position: absolute;
                top: 0;
                right: 0;
                bottom: 0;
                left: 0;
                /* background for toolbar when it is at its full size */
                --paper-scroll-header-panel-full-header: {
                    <?php if($headerImage = self::getAriesPolymerThemeMod('ariespolymer_header_image') ): ?>
                    background-image: url(<?php echo $headerImage ?>);
                    <?php else : ?>
                    background-color: <?php echo self::getAriesPolymerThemeMod('ariespolymer_header_background','#253c7c')?>;
                    <?php endif ?>
                };
                /* background for toolbar when it is condensed */
                --paper-scroll-header-panel-condensed-header: {
                    background-color: <?php echo self::getAriesPolymerThemeMod('ariespolymer_header_background','#253c7c')?>;
                };
            }
           paper-toolbar#mainToolbar {
                background-color: transparent;
                overflow: visible;
            }
            #drawer paper-menu{
                --paper-menu-background-color: <?php echo self::getAriesPolymerThemeMod('ariespolymer_menu_siderbar_background','#ffffff')?>;
                --paper-menu-color: <?php echo self::getAriesPolymerThemeMod('ariespolymer_menu_anchor_color','#191919')?>;
                --paper-menu-selected-item:{
                    --paper-menu-background-color: <?php echo self::getAriesPolymerThemeMod('ariespolymer_menu_siderbar_background','#ffffff')?>;
                    background-color: <?php echo self::getAriesPolymerThemeMod('ariespolymer_menu_siderbar_background','#253c7c')?>;
                }
                --paper-menu-focused-item:{
                    --paper-menu-background-color: <?php echo self::getAriesPolymerThemeMod('ariespolymer_menu_siderbar_background','#ffffff')?>;
                    background-color: <?php echo self::getAriesPolymerThemeMod('ariespolymer_menu_siderbar_background','#253c7c')?>;
                }
                --paper-menu-focused-item-after:{
                    --paper-menu-background-color: <?php echo self::getAriesPolymerThemeMod('ariespolymer_menu_siderbar_background','#ffffff')?>;
                    background-color: <?php echo self::getAriesPolymerThemeMod('ariespolymer_menu_siderbar_background','#253c7c')?>;
                }
            }
        </style>
        <style>

            html {
                font-size: <?php echo self::getAriesPolymerThemeMod('ariespolymer_body_font_size','16')?>px;
            }

            <?php if(get_theme_mod( 'ariespolymer_body_font' )  && get_theme_mod( 'ariespolymer_body_font' ) != 'Roboto' ):?>
            <?php echo $fonts[get_theme_mod( 'ariespolymer_body_font' )]['import'];?>
            <?php endif ?>
            <?php if(get_theme_mod( 'ariespolymer_menu_font' ) && get_theme_mod( 'ariespolymer_menu_font' ) != 'Roboto' ):?>
            <?php echo $fonts[get_theme_mod( 'ariespolymer_menu_font' )]['import'];?>
            <?php endif ?>

            body{
            <?php if(get_theme_mod( 'ariespolymer_body_font' ) ):?>
                <?php echo $fonts[get_theme_mod( 'ariespolymer_body_font' )]['css'];?>
            <?php else: ?>
                font-family: 'Roboto', sans-serif;;
            <?php endif ?>
            }
            <?php $defaultHeadlineFontSize = array(1 => 30, 2 => 25, 3 => 16, 4 => 16, 5 => 16, 6 => 16); ?>
            <?php foreach(range(1,6) as $h): ?>
            h<?php echo $h ?>{
                font-size: <?php echo self::getAriesPolymerThemeMod('ariespolymer_'.$h.'_font_size', $defaultHeadlineFontSize[$h])?>px;
            }
            <?php endforeach ?>
            a, a:active, a:visited, a:focus {
                color: <?php echo self::getAriesPolymerThemeMod('ariespolymer_links_color','#0645ad')?>;
            }

            .polymer-top-menu li{
            <?php if(get_theme_mod( 'ariespolymer_menu_font' ) ):?>
            <?php echo $fonts[get_theme_mod( 'ariespolymer_menu_font' )]['css'];?>
            <?php else: ?>
                font-family: 'Roboto', sans-serif;;
            <?php endif ?>
            }
            paper-toolbar.paper-toolbar-0 {
                background: <?php echo self::getAriesPolymerThemeMod('ariespolymer_header_background','#253c7c')?> none repeat scroll 0 0;
                color: <?php echo self::getAriesPolymerThemeMod('ariespolymer_header_color','#ffffff') ?>;
            }
            .paper-drawer-panel-0 iron-selector.paper-drawer-panel > #drawer.paper-drawer-panel, paper-toolbar.paper-toolbar-0#drawerToolbar{
                background: <?php echo self::getAriesPolymerThemeMod('ariespolymer_menu_siderbar_background','#ffffff')?> none repeat scroll 0 0;
            }
            .footer-media-links.pw-footer {
                background: <?php echo self::getAriesPolymerThemeMod('ariespolymer_footer_sm_background','#d2f7f1')?> none repeat scroll 0 0;
            }
            #main, #main .aries-polymer-nav{
                background: <?php echo self::getAriesPolymerThemeMod('ariespolymer_body_background','#ffffff') ?> none repeat scroll 0 0;
                color: <?php echo self::getAriesPolymerThemeMod('ariespolymer_body_color','#191919')?>;
            }
            .footer, .footer paper-menu, .footer paper-material  {
                background: <?php echo self::getAriesPolymerThemeMod('ariespolymer_footer_background','#1C1A26')?> none repeat scroll 0 0;
            }
            .footer, .footer a, .footer h1, .footer h2, .footer h3, .footer h4, .footer h5, .footer h6, .footer paper-menu a:not([style-scope]):not(.style-scope)  {
                color: <?php echo self::getAriesPolymerThemeMod('ariespolymer_footer_color','#ffffff')?>;
            }
            #menu-hauptmenu a, #menu-hauptmenu a:active, #menu-hauptmenu a:visited, #menu-hauptmenu a:focus {
                color: <?php echo self::getAriesPolymerThemeMod('ariespolymer_menu_anchor_color','#191919')?>;
                font-size: <?php echo self::getAriesPolymerThemeMod('ariespolymer_menu_font_size','12')?>px;
            }
            .menu-sidebar *:not(a) {
                color: <?php echo self::getAriesPolymerThemeMod('ariespolymer_menu_siderbar_color','#191919')?>;
            }
            #menu-hauptmenu .current-menu-item > a {
                color: <?php echo self::getAriesPolymerThemeMod('ariespolymer_menu_anchor_active_color','#3f51b5')?>;
            }
            .footer-line{ border-bottom-color: <?php echo self::getAriesPolymerThemeMod('ariespolymer_footer_top_line_color','#f22c5a')?>;}
            .polymer-top-menu .current-menu-item{
                border-left: 4px solid <?php echo self::getAriesPolymerThemeMod('ariespolymer_menu_active_border_color','#f22c5a')?>;
            }
            .copyright {
                background: <?php echo self::getAriesPolymerThemeMod('ariespolymer_copyright_background','#000000') ?> none repeat scroll 0 0;
            }
            .copyright, .copyright a, .copyright h1, .copyright h2, .copyright h3, .copyright h4, .copyright h5, .copyright h6  {
                color: <?php echo self::getAriesPolymerThemeMod('ariespolymer_copyright_color','#ffffff') ?>;
            }

            .home-logo {
                height: 100%;
                width: 300px;
                display: block;
                background: url(<?php echo self::getAriesPolymerThemeMod('ariespolymer_logo') ?>) no-repeat left center;
                background-size: auto 100%;
            }
            .pagination-link, .pagination-link:hover{
                color: #191919;
                text-decoration: none;
            }

        </style>
    <?php
    }

    public static  function getAriesPolymerThemeMod($theme_mod, $default = false){
        return (get_theme_mod( $theme_mod ) ) ? get_theme_mod( $theme_mod )  : $default;
    }
}
