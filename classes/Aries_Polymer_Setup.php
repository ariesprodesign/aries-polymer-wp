<?php

/**
 * Class Aries_Polymer_Setup
 */
class Aries_Polymer_Setup {

    /**
     * Aries_Polymer_Setup constructor.
     */
    function __construct() {
        add_action( 'customize_register', array($this, 'ariespolymer_theme_customizer'));
        add_action( 'widgets_init', array($this, 'ariespolymer_widgets_init') );
        add_action( 'after_setup_theme', array($this, 'aries_polymer_after_setup_theme') );
        add_action( 'customize_register', array($this, 'customize_remove_components'), 100);
        add_filter('widget_text','do_shortcode');
        add_action( 'wp_head', array($this, 'importPolymerElements') );
        add_filter('next_posts_link_attributes',  array($this, 'ariesPolymerPaginationLinkAttributes'));
        add_filter('previous_posts_link_attributes',  array($this, 'ariesPolymerPaginationLinkAttributes'));
    }

    /**
     * @return string
     */
    public function ariesPolymerPaginationLinkAttributes(){
        return 'class="pagination-link"';
    }


    /**
     *
     */
    public function importPolymerElements() {

        $available_imports = array('/<google-youtube.*>.*<\/google-youtube>/s' => '/bower_components/google-youtube/google-youtube.html',
                                   '/<iron-a11y-announcer.*>.*<\/iron-a11y-announcer>/s' => '/bower_components/iron-a11y-announcer/iron-a11y-announcer.html',
                                   '/<iron-a11y-keys.*>.*<\/iron-a11y-keys>/s' => '/bower_components/iron-a11y-keys/iron-a11y-keys.html',
                                   '/<iron-ajax.*>.*<\/iron-ajax>/s' => '/bower_components/iron-ajax/iron-ajax.html',
                                   '/<iron-autogrow-textarea.*>.*<\/iron-autogrow-textarea>/s' => '/bower_components/iron-autogrow-textarea/iron-autogrow-textarea.html',
                                   '/<iron-doc-viewer.*>.*<\/iron-doc-viewer>/s' => '/bower_components/iron-doc-viewer/iron-doc-viewer.html',
                                   '/<iron-dropdown.*>.*<\/iron-dropdown>/s' => '/bower_components/iron-dropdown/iron-dropdown.html',
                                   '/<iron-jsonp-library.*>.*<\/iron-jsonp-library>/s' => '/bower_components/iron-jsonp-library/iron-jsonp-library.html',
                                   '/<iron-label.*>.*<\/iron-label>/s' => '/bower_components/iron-label/iron-label.html',
                                   '/<iron-list.*>.*<\/iron-list>/s' => '/bower_components/iron-list/iron-list.html',
                                   '/<iron-localstorage.*>.*<\/iron-localstorage>/s' => '/bower_components/iron-localstorage/iron-localstorage.html',
                                   '/<iron-media-query.*>.*<\/iron-media-query>/s' => '/bower_components/iron-media-query/iron-media-query.html',
                                   '/<iron-pages.*>.*<\/iron-pages>/s' => '/bower_components/iron-pages/iron-pages.html',
                                   '/<iron-swipeable-container.*>.*<\/iron-swipeable-container>/s' => '/bower_components/iron-swipeable-container/iron-swipeable-container.html',
                                   '/<paper-badge.*>.*<\/paper-badge>/s' => '/bower_components/paper-badge/paper-badge.html',
                                   '/<paper-checkbox.*>.*<\/paper-checkbox>/s' => '/bower_components/paper-checkbox/paper-checkbox.html',
                                   '/<paper-dialog-scrollable.*>.*<\/paper-dialog-scrollable>/s' => '/bower_components/paper-dialog-scrollable/paper-dialog-scrollable.html',
                                   '/<paper-dropdown-menu.*>.*<\/paper-dropdown-menu>/s' => '/bower_components/paper-dropdown-menu/paper-dropdown-menu.html',
                                   '/<paper-fab.*>.*<\/paper-fab>/s' => '/bower_components/paper-fab/paper-fab.html',
                                   '/<paper-listbox.*>.*<\/paper-listbox>/s' => '/bower_components/paper-listbox/paper-listbox.html',
                                   '/<paper-menu-button.*>.*<\/paper-menu-button>/s' => '/bower_components/paper-menu-button/paper-menu-button.html',
                                   '/<paper-progress.*>.*<\/paper-progress>/s' => '/bower_components/paper-menu-button/paper-progress.html',
                                   '/<paper-radio-button.*>.*<\/paper-radio-button>/s' => '/bower_components/paper-radio-button/paper-radio-button.html',
                                   '/<paper-radio-group.*>.*<\/paper-radio-group>/s' => '/bower_components/paper-radio-group/paper-radio-group.html',
                                   '/<paper-ripple.*>.*<\/paper-ripple>/s' => '/bower_components/paper-ripple/paper-ripple.html',
                                   '/<paper-slider.*>.*<\/paper-slider>/s' => '/bower_components/paper-slider/paper-slider.html',
                                   '/<paper-spinner.*>.*<\/paper-spinner>/s' => '/bower_components/paper-spinner/paper-spinner.html',
                                   '/<paper-tabs.*>.*<\/paper-tabs>/s' => '/bower_components/paper-tabs/paper-tabs.html',
                                   '/<paper-toggle-button.*>.*<\/paper-toggle-button>/s' => '/bower_components/paper-toggle-button/paper-toggle-button.html',
                                   '/<paper-tooltip.*>.*<\/paper-tooltip>/s' => '/bower_components/paper-tooltip/paper-tooltip.html');

        $post_id = get_queried_object_id();
        $content = get_post_field('post_content', $post_id);
        foreach($available_imports as $key => $item){
            if(preg_match($key, $content)){
                echo '<link rel="import" href="'.get_template_directory_uri().$item.'"/>';
            }
        }
    }

    /**
     * @param $wp_customize
     */
    public function customize_remove_components($wp_customize){
        $wp_customize->get_panel( 'nav_menus' )->active_callback = '__return_false';
    }


    /**
     *
     */
    public function ariespolymer_widgets_init() {
        register_sidebar( array(
                              'name'          => __( 'Menu Sidebar', 'ariespolymer' ),
                              'id'            => 'sidebar-1',
                              'description'   => __( 'Add widgets here to appear in your Menu sidebar.', 'ariespolymer' ),
                              'before_widget' => '<section id="%1$s" class="widget %2$s">',
                              'after_widget'  => '</section>',
                              'before_title'  => '<h2 class="widget-title">',
                              'after_title'   => '</h2>',
                          ) );

        register_sidebar( array(
                              'name'          => __( 'Sidebar Right', 'ariespolymer' ),
                              'id'            => 'sidebar-5',
                              'description'   => __( 'Add widgets here to appear in your right sidebar.', 'ariespolymer' ),
                              'before_widget' => '<section id="%1$s" class="widget %2$s">',
                              'after_widget'  => '</section>',
                              'before_title'  => '<h2 class="widget-title">',
                              'after_title'   => '</h2>',
                          ) );

        register_sidebar( array(
                              'name'          => __( 'Footer 1', 'ariespolymer' ),
                              'id'            => 'sidebar-2',
                              'description'   => __( 'Add widgets here to appear in your footer.', 'ariespolymer' ),
                              'before_widget' => '<section id="%1$s" class="widget %2$s">',
                              'after_widget'  => '</section>',
                              'before_title'  => '<h2 class="widget-title">',
                              'after_title'   => '</h2>',
                          ) );

        register_sidebar( array(
                              'name'          => __( 'Footer 2', 'ariespolymer' ),
                              'id'            => 'sidebar-3',
                              'description'   => __( 'Add widgets here to appear in your footer.', 'ariespolymer' ),
                              'before_widget' => '<section id="%1$s" class="widget %2$s">',
                              'after_widget'  => '</section>',
                              'before_title'  => '<h2 class="widget-title">',
                              'after_title'   => '</h2>',
                          ) );

        register_sidebar( array(
                              'name'          => __( 'Footer 3', 'ariespolymer' ),
                              'id'            => 'sidebar-4',
                              'description'   => __( 'Add widgets here to appear in your footer.', 'ariespolymer' ),
                              'before_widget' => '<section id="%1$s" class="widget %2$s">',
                              'after_widget'  => '</section>',
                              'before_title'  => '<h2 class="widget-title">',
                              'after_title'   => '</h2>',
                          ) );
    }

    /**
     *
     */
    public function aries_polymer_after_setup_theme() {

        load_theme_textdomain( 'ariespolymer', get_template_directory() . '/languages' );

        add_theme_support( 'automatic-feed-links' );

        add_theme_support( 'title-tag' );

        add_theme_support( 'post-thumbnails' );

        add_image_size( 'ariespolymer-featured-image', 2000, 1200, true );

        add_image_size( 'ariespolymer-thumbnail-avatar', 100, 100, true );

        // This theme uses wp_nav_menu() in two locations.
        register_nav_menus( array(
                                'primary'    => __( 'Primary Menu', 'ariespolymer' ),
                                'footer' => __( 'Footer Menu', 'ariespolymer' ),
                            ) );


        add_theme_support( 'html5', array(
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        ) );


        add_theme_support( 'post-formats', array(
            'aside',
            'image',
            'video',
            'quote',
            'link',
            'gallery',
            'audio',
        ) );

        add_post_type_support( 'page', 'post-formats' );

        //add_theme_support( 'customize-selective-refresh-widgets' );


    }

    /**
     * @return mixed|void
     */
    public static function get_my_available_fonts() {
        $sansSerif = array('ABeeZee','Abel','Aclonica','Acme','Actor','Advent Pro','Aldrich','Alef','Alegreya Sans','Alegreya Sans SC','Allerta','Allerta Stencil','Amaranth','Amiko','Anaheim','Andika','Antic','Anton','Archivo Black','Archivo Narrow','Arimo','Armata','Arya','Asap','Assistant','Asul','Athiti','Average Sans','Basic','Belleza','BenchNine','Biryani','Bubbler One','Cagliostro','Cairo','Cambay','Candal','Cantarell','Cantora One','Capriola','Carme','Carrois Gothic','Carrois Gothic SC','Catamaran','Changa','Chathura','Chau Philomene One','Chivo','Coda Caption','Convergence','Cuprum','Days One','Denk One','Dhurjati','Didact Gothic','Doppio One','Dorsa','Dosis','Droid Sans','Duru Sans','Economica','Ek Mukta','El Messiri','Electrolize','Englebert','Exo','Exo 2','Federo','Fira Sans','Fjalla One','Francois One','Frank Ruhl Libre','Fresca','GFS Neohellenic','Gafata','Gidugu','Gudea','Hammersmith One','Harmattan','Heebo','Hind','Hind Guntur','Hind Madurai','Hind Siliguri','Hind Vadodara','Homenaje','Imprima','Inder','Istok Web','Jaldi','Jockey One','Josefin Sans','Julius Sans One','Jura','Kanit','Kantumruy','Karla','Khand','Khula','Kite One','Krona One','Lato','Lekton','Magra','Mako','Mallanna','Mandali','Marmelad','Martel Sans','Marvel','Maven Pro','Meera Inimai','Merriweather Sans','Metrophobic','Michroma','Miriam Libre','Mitr','Molengo','Monda','Montserrat','Montserrat Alternates','Montserrat Subrayada','Mouse Memoirs','Mukta Vaani','Muli','NTR','News Cycle','Nobile','Noto Sans','Numans','Nunito','Nunito Sans','Open Sans','Open Sans Condensed','Orbitron','Orienta','Oswald','Oxygen','PT Sans','PT Sans Caption','PT Sans Narrow','Palanquin','Palanquin Dark','Pathway Gothic One','Pattaya','Pavanam','Paytone One','Philosopher','Play','Pontano Sans','Poppins','Port Lligat Sans','Pragati Narrow','Prompt','Proza Libre','Puritan','Quantico','Quattrocento Sans','Questrial','Quicksand','Rajdhani','Raleway','Ramabhadra','Rambla','Rationale','Reem Kufi','Roboto','Roboto Condensed','Ropa Sans','Rosario','Rubik','Rubik Mono One','Rubik One','Rum Raisin','Russo One','Scada','Secular One','Seymour One','Shanti','Share Tech','Signika','Signika Negative','Sintony','Six Caps','Snippet','Source Sans Pro','Spinnaker','Strait','Syncopate','Tauri','Teko','Telex','Tenali Ramakrishna','Tenor Sans','Text Me One','Timmana','Titillium Web','Ubuntu','Ubuntu Condensed','Varela','Varela Round','Viga','Voltaire','Wendy One','Wire One','Work Sans','Yanone Kaffeesatz','Yantramanav');
        $serif = array('Abhaya Libre','Adamina','Alegreya','Alegreya SC','Alice','Alike','Alike Angular','Almendra','Almendra SC','Amethysta','Amiri','Andada','Antic Didone','Antic Slab','Arapey','Arbutus Slab','Aref Ruqaa','Artifika','Arvo','Asar','Average','Balthazar','Belgrano','Bentham','BioRhyme','BioRhyme Expanded','Bitter','Brawler','Bree Serif','Buenard','Cambo','Cantata One','Cardo','Caudex','Cinzel','Copse','Cormorant','Cormorant Garamond','Cormorant Infant','Cormorant SC','Cormorant Unicase','Cormorant Upright','Coustard','Crete Round','Crimson Text','Cutive','David Libre','Della Respira','Domine','Donegal One','Droid Serif','EB Garamond','Eczar','Enriqueta','Esteban','Fanwood Text','Fasthand','Fauna One','Fenix','Fjord One','GFS Didot','Gabriela','Gentium Basic','Gentium Book Basic','Gilda Display','Glegoo','Goudy Bookletter 1911','Gurajada','Habibi','Halant','Hanuman','Headland One','Holtwood One SC','IM Fell DW Pica','IM Fell DW Pica SC','IM Fell Double Pica','IM Fell Double Pica SC','IM Fell English','IM Fell English SC','IM Fell French Canon','IM Fell French Canon SC','IM Fell Great Primer','IM Fell Great Primer SC','Inika','Inknut Antiqua','Inika','Inknut Antiqua','Italiana','Jacques Francois','Josefin Slab','Judson','Junge','Kadwa','Kameron','Karma','Kotta One','Kreon','Kurale','Laila','Ledger','Libre Baskerville','Linden Hill','Lora','Lusitana','Lustria','Maitree','Marcellus','Marcellus SC','Marko One','Martel','Mate','Mate SC','Merriweather','Montaga','Neuton','Nokora','Noticia Text','Noto Serif','Old Standard TT','Oranienbaum','Ovo','PT Serif','PT Serif Caption','Peddana','Petrona','Playfair Display','Playfair Display SC','Podkova','Poly','Port Lligat Slab','Prata','Pridi','Prociono','Quando','Quattrocento','Radley','Ramaraja','Rasa','Rhodium Libre','Roboto Slab','Rokkitt','Rosarivo','Rozha One','Rufina','Sahitya','Sanchez','Scheherazade','Scope One','Slabo 13px','Slabo 27px','Sorts Mill Goudy','Source Serif Pro','Sree Krushnadevaraya','Stoke','Suez One','Sumana','Sura','Suranna','Suravaram','Taviraj','Tienne','Tinos','Trirong','Trocchi','Trykker','Ultra','Unna','Vesper Libre','Vidaloka','Volkhov','Vollkorn','Yrsa');
        $fonts = array(
            'arial' => array(
                'name' => 'Arial',
                'import' => '',
                'css' => "font-family: Arial, sans-serif;",
                'type' => 'Sans Serif'
            ),
            'arial-black' => array(
                'name' => 'Arial Black',
                'import' => '',
                'css' => "font-family: \"Arial Black\", sans-serif;",
                'type' => 'Sans Serif'
            ),
            'courier-new' => array(
                'name' => 'Courier New',
                'import' => '',
                'css' => "font-family: \"Courier New\", monospace;",
                'type' => 'Monospace'
            ),
            'georgia' => array(
                'name' => 'Georgia',
                'import' => '',
                'css' => "font-family: Georgia, serif;",
                'type' => 'Serif'
            ),
            'lucida-console' => array(
                'name' => 'Lucida Console',
                'import' => '',
                'css' => "font-family: \"Lucida Console\", monospace;",
                'type' => 'Monospace'
            ),
            'tahoma' => array(
                'name' => 'Tahoma',
                'import' => '',
                'css' => "font-family: Tahoma, sans-serif;",
                'type' => 'Sans Serif'
            ),
            'times-new-roman' => array(
                'name' => 'Times New Roman',
                'import' => '',
                'css' => "font-family: \"Times New Roman\", serif;",
                'type' => 'Serif'
            ),
            'verdana' => array(
                'name' => 'Verdana',
                'import' => '',
                'css' => "font-family: Verdana, sans-serif;",
                'type' => 'Sans Serif'
            )
        );

        foreach($sansSerif as $item){
            $fonts[self::create_key_slug($item)] = array('name' => $item,
                                                          'import' => '@import url(https://fonts.googleapis.com/css?family='.str_replace(' ','+',$item).');',
                                                          'css' => "font-family: '".$item."', sans-serif;",
                                                          'type' => 'Sans Serif');

        }
        foreach($serif as $item){
            $fonts[self::create_key_slug($item)] = array('name' => $item,
                                                          'import' => '@import url(https://fonts.googleapis.com/css?family='.str_replace(' ','+',$item).');',
                                                          'css' => "font-family: '".$item."', serif;",
                                                          'type' => 'Serif');

        }

        ksort($fonts);

        return apply_filters( 'aries_polymer_available_fonts', $fonts );
    }

    /**
     * @param $string
     * @return mixed
     */
    public static function create_key_slug($string){
        $slug=preg_replace('/[^A-Za-z0-9-]+/', '-', strtolower($string));
        return $slug;
    }


    /**
     * @param $wp_customize
     */
    public function ariespolymer_theme_customizer($wp_customize ) {

        $fonts = array();

        foreach(self::get_my_available_fonts() as $key => $item){
            $fonts[$key] = $item['name'].' ('. $item['type'].')';
        }

        //Add Logo
        $wp_customize->add_setting( 'ariespolymer_logo' );
        $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'ariespolymer_logo', array(
            'label'    => __( 'Logo', 'ariespolymer' ),
            'section'  => 'title_tagline',
            'settings' => 'ariespolymer_logo',
        ) ) );

        $wp_customize->add_setting( 'ariespolymer_header_image' );
        $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'ariespolymer_header_image', array(
            'label'    => __( 'Header Image', 'ariespolymer' ),
            'section'  => 'title_tagline',
            'settings' => 'ariespolymer_header_image',
        ) ) );

        //Section for Colors
        $wp_customize->add_section( 'ariespolymer_general_section' , array(
            'title'       => __( 'General', 'ariespolymer' ),
            'priority'    => 25,
            'description' => __( 'Set the color you would like to use.', 'ariespolymer' ),
        ) );

        $wp_customize->add_setting('ariespolymer_body_font', array('default'     => 'Roboto'));
        $wp_customize->add_control( 'ariespolymer_body_font', array(
            'label'    => __( 'Body Font Family', 'ariespolymer' ),
            'section'  => 'ariespolymer_general_section',
            'settings' => 'ariespolymer_body_font',
            'type'     => 'select',
            'choices'  => $fonts,
            'priority'   => 101,
        )  );

        $wp_customize->add_setting('ariespolymer_body_font_size', array('default'     => '16'));
        $wp_customize->add_control( 'ariespolymer_body_font_size', array(
            'label'    => __( 'Body Font Size in pixel', 'ariespolymer' ),
            'section'  => 'ariespolymer_general_section',
            'settings' => 'ariespolymer_body_font_size',
            'type'     => 'select',
            'choices'  => array_combine(range(5,50), range(5,50)),
            'priority'   => 102,
        )  );

        $wp_customize->add_setting('ariespolymer_menu_font', array('default'     => 'Roboto'));
        $wp_customize->add_control( 'ariespolymer_menu_font', array(
            'label'    => __( 'Menu Font Family', 'ariespolymer' ),
            'section'  => 'ariespolymer_general_section',
            'settings' => 'ariespolymer_menu_font',
            'type'     => 'select',
            'choices'  => $fonts,
            'priority'   => 103,
        )  );

        $wp_customize->add_setting('ariespolymer_menu_font_size', array('default'     => '12'));
        $wp_customize->add_control( 'ariespolymer_menu_font_size', array(
            'label'    => __( 'Menu Font Size in pixel', 'ariespolymer' ),
            'section'  => 'ariespolymer_general_section',
            'settings' => 'ariespolymer_menu_font_size',
            'type'     => 'select',
            'choices'  => array_combine(range(5,50), range(5,50)),
            'priority'   => 104,
        )  );

        $wp_customize->add_setting('ariespolymer_post_page_title', array('default'     => 'Post'));
        $wp_customize->add_control( 'ariespolymer_post_page_title', array(
            'label'    => __( 'Post page title', 'ariespolymer' ),
            'section'  => 'ariespolymer_general_section',
            'settings' => 'ariespolymer_post_page_title',
            'priority'   => 105,
        )  );

        $wp_customize->add_setting('ariespolymer_footer_nav_title', array('default'     => ''));
        $wp_customize->add_control( 'ariespolymer_footer_nav_title', array(
            'label'    => __( 'Footer links title', 'ariespolymer' ),
            'section'  => 'ariespolymer_general_section',
            'settings' => 'ariespolymer_footer_nav_title',
            'priority'   => 106,
        )  );

        $wp_customize->add_setting('ariespolymer_footer_copyright', array('default'     => '©'. date("Y").' Aries Prodesign'));
        $wp_customize->add_control( 'ariespolymer_footer_copyright', array(
            'label'    => __( 'Copyright', 'ariespolymer' ),
            'section'  => 'ariespolymer_general_section',
            'settings' => 'ariespolymer_footer_copyright',
            'priority'   => 107,
        )  );

        $wp_customize->add_setting('ariespolymer_default_gallery', array('default'     => 'polymer'));
        $wp_customize->add_control( 'ariespolymer_default_gallery', array(
            'label'    => __( 'Default Gallery', 'ariespolymer' ),
            'section'  => 'ariespolymer_general_section',
            'settings' => 'ariespolymer_default_gallery',
            'type'     => 'radio',
            'choices'  => array(
                'polymer'  => __( 'Polymer Gallery', 'ariespolymer' ),
                'wordpress' => __( 'WordPress Gallery', 'ariespolymer' ),
            ),
            'priority'   => 108,
        )  );

        //Section for Colors
        $wp_customize->add_section( 'ariespolymer_color_section' , array(
            'title'       => __( 'Colors', 'ariespolymer' ),
            'priority'    => 27,
            'description' => __( 'Set the color you would like to use.', 'ariespolymer' ),
        ) );
        $wp_customize->add_setting('ariespolymer_links_color', array('default'     => '#0645ad'));
        $wp_customize->add_setting('ariespolymer_header_background', array('default'     => '#253c7c'));
        $wp_customize->add_setting('ariespolymer_header_color', array('default'     => '#ffffff'));
        $wp_customize->add_setting('ariespolymer_body_background', array('default'     => '#ffffff'));
        $wp_customize->add_setting('ariespolymer_body_color', array('default'     => '#191919'));
        $wp_customize->add_setting('ariespolymer_footer_sm_background', array('default'     => '#d2f7f1'));
        $wp_customize->add_setting('ariespolymer_footer_background', array('default'     => '#1C1A26'));
        $wp_customize->add_setting('ariespolymer_footer_color', array('default'     => '#ffffff'));
        $wp_customize->add_setting('ariespolymer_footer_top_line_color', array('default'     => '#f22c5a'));
        $wp_customize->add_setting('ariespolymer_copyright_background', array('default'     => '#000000'));
        $wp_customize->add_setting('ariespolymer_copyright_color', array('default'     => '#ffffff'));

        $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize,'ariespolymer_links_color',array(
            'label'      => __( 'Links Color', 'ariespolymer' ),
            'section'    => 'ariespolymer_color_section',
            'settings'   => 'ariespolymer_links_color'
        )));
        $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize,'ariespolymer_header_background',array(
            'label'      => __( 'Header Background', 'ariespolymer' ),
            'section'    => 'ariespolymer_color_section',
            'settings'   => 'ariespolymer_header_background'
        )));
        $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'ariespolymer_header_color', array(
            'label'      => __( 'Header Color', 'ariespolymer' ),
            'section'    => 'ariespolymer_color_section',
            'settings'   => 'ariespolymer_header_color'
        )));
        $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize,'ariespolymer_body_background',array(
            'label'      => __( 'Content Background', 'ariespolymer' ),
            'section'    => 'ariespolymer_color_section',
            'settings'   => 'ariespolymer_body_background'
        )));
        $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize,'ariespolymer_body_color',array(
            'label'      => __( 'Content Color', 'ariespolymer' ),
            'section'    => 'ariespolymer_color_section',
            'settings'   => 'ariespolymer_body_color'
        )));
        $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize,'ariespolymer_footer_sm_background',array(
            'label'      => __( 'Footer Social Media Background', 'ariespolymer' ),
            'section'    => 'ariespolymer_color_section',
            'settings'   => 'ariespolymer_footer_sm_background'
        )));
        $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize,'ariespolymer_footer_background',array(
            'label'      => __( 'Footer Background', 'ariespolymer' ),
            'section'    => 'ariespolymer_color_section',
            'settings'   => 'ariespolymer_footer_background'
        )));
        $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize,'ariespolymer_footer_color',array(
            'label'      => __( 'Footer Color', 'ariespolymer' ),
            'section'    => 'ariespolymer_color_section',
            'settings'   => 'ariespolymer_footer_color'
        )));
        $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize,'ariespolymer_footer_top_line_color',array(
            'label'      => __( 'Footer Top Line Color', 'ariespolymer' ),
            'section'    => 'ariespolymer_color_section',
            'settings'   => 'ariespolymer_footer_top_line_color'
        )));

        $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize,'ariespolymer_copyright_background',array(
            'label'      => __( 'Footer Copyright Background', 'ariespolymer' ),
            'section'    => 'ariespolymer_color_section',
            'settings'   => 'ariespolymer_copyright_background'
        )));
        $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize,'ariespolymer_copyright_color',array(
            'label'      => __( 'Footer Copyright Color', 'ariespolymer' ),
            'section'    => 'ariespolymer_color_section',
            'settings'   => 'ariespolymer_copyright_color'
        )));

        //Section for Main Menu Colors
        $wp_customize->add_section( 'ariespolymer_menu_color_section' , array(
            'title'       => __( 'Main Menu Colors', 'ariespolymer' ),
            'priority'    => 28,
            'description' => __( 'Set the color you would like to use.', 'ariespolymer' ),
        ) );

        $wp_customize->add_setting('ariespolymer_menu_siderbar_background', array('default'     => '#ffffff'));
        $wp_customize->add_setting('ariespolymer_menu_siderbar_color', array('default'     => '#191919'));
        $wp_customize->add_setting('ariespolymer_menu_anchor_color', array('default'     => '#191919'));
        $wp_customize->add_setting('ariespolymer_menu_anchor_active_color', array('default'     => '#3f51b5'));
        $wp_customize->add_setting('ariespolymer_menu_active_border_color', array('default'     => '#f22c5a'));
        $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize,'ariespolymer_menu_siderbar_background',array(
            'label'      => __( 'Menu Sidebar Background', 'ariespolymer' ),
            'section'    => 'ariespolymer_menu_color_section',
            'settings'   => 'ariespolymer_menu_siderbar_background'
        )));
        $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize,'ariespolymer_menu_siderbar_color',array(
            'label'      => __( 'Menu Sidebar Color', 'ariespolymer' ),
            'section'    => 'ariespolymer_menu_color_section',
            'settings'   => 'ariespolymer_menu_siderbar_color'
        )));
        $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize,'ariespolymer_menu_anchor_color',array(
            'label'      => __( 'Menu Anchor Color', 'ariespolymer' ),
            'section'    => 'ariespolymer_menu_color_section',
            'settings'   => 'ariespolymer_menu_anchor_color'
        )));
        $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize,'ariespolymer_menu_anchor_active_color',array(
            'label'      => __( 'Active Menu Anchor Color', 'ariespolymer' ),
            'section'    => 'ariespolymer_menu_color_section',
            'settings'   => 'ariespolymer_menu_anchor_active_color'
        )));
        $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize,'ariespolymer_menu_active_border_color',array(
            'label'      => __( 'Active Menu Item Border', 'ariespolymer' ),
            'section'    => 'ariespolymer_menu_color_section',
            'settings'   => 'ariespolymer_menu_active_border_color'
        )));

        //Section for Social Medias
        $wp_customize->add_section( 'ariespolymer_social_media_section' , array(
            'title'       => __( 'Social Media', 'ariespolymer' ),
            'priority'    => 28,
            'description' => __( 'Set your Social Media urls', 'ariespolymer' ),
        ) );
        $wp_customize->add_setting('ariespolymer_social_facebook', array('default' => ''));
        $wp_customize->add_control('ariespolymer_social_facebook', array(
            'label'      => __('Facebook Url', 'ariespolymer'),
            'section'    => 'ariespolymer_social_media_section',
            'settings'   => 'ariespolymer_social_facebook',
        ));
        $wp_customize->add_setting('ariespolymer_social_twitter', array('default' => ''));
        $wp_customize->add_control('ariespolymer_social_twitter', array(
            'label'      => __('Twitter Url', 'ariespolymer'),
            'section'    => 'ariespolymer_social_media_section',
            'settings'   => 'ariespolymer_social_twitter',
        ))
        ;
        $wp_customize->add_setting('ariespolymer_social_google', array('default' => ''));
        $wp_customize->add_control('ariespolymer_social_google', array(
            'label'      => __('Google Plus Url', 'ariespolymer'),
            'section'    => 'ariespolymer_social_media_section',
            'settings'   => 'ariespolymer_social_google',
        ));
        $wp_customize->add_setting('ariespolymer_social_instagram', array('default' => ''));
        $wp_customize->add_control('ariespolymer_social_instagram', array(
            'label'      => __('Instagram Url', 'ariespolymer'),
            'section'    => 'ariespolymer_social_media_section',
            'settings'   => 'ariespolymer_social_instagram',
        ));
        $wp_customize->add_setting('ariespolymer_social_linkedin', array('default' => ''));
        $wp_customize->add_control('ariespolymer_social_linkedin', array(
            'label'      => __('LinkedIn Url', 'ariespolymer'),
            'section'    => 'ariespolymer_social_media_section',
            'settings'   => 'ariespolymer_social_linkedin',
        ));
        $wp_customize->add_setting('ariespolymer_social_pinterest', array('default' => ''));
        $wp_customize->add_control('ariespolymer_social_pinterest', array(
            'label'      => __('Pinterest Url', 'ariespolymer'),
            'section'    => 'ariespolymer_social_media_section',
            'settings'   => 'ariespolymer_social_pinterest',
        ));
        $wp_customize->add_setting('ariespolymer_social_youtube', array('default' => ''));
        $wp_customize->add_control('ariespolymer_social_youtube', array(
            'label'      => __('Youtube Url', 'ariespolymer'),
            'section'    => 'ariespolymer_social_media_section',
            'settings'   => 'ariespolymer_social_youtube',
        ));
        $wp_customize->add_setting('ariespolymer_social_vimeo', array('default' => ''));
        $wp_customize->add_control('ariespolymer_social_vimeo', array(
            'label'      => __('Vimeo Url', 'ariespolymer'),
            'section'    => 'ariespolymer_social_media_section',
            'settings'   => 'ariespolymer_social_vimeo',
        ));
        $wp_customize->add_setting('ariespolymer_social_tumblr', array('default' => ''));
        $wp_customize->add_control('ariespolymer_social_tumblr', array(
            'label'      => __('Tumblr Url', 'ariespolymer'),
            'section'    => 'ariespolymer_social_media_section',
            'settings'   => 'ariespolymer_social_tumblr',
        ));

        //Section for Colors
        $wp_customize->add_section( 'ariespolymer_headlines_section' , array(
            'title'       => __( 'Headlines', 'ariespolymer' ),
            'priority'    => 26,
            'description' => __( 'Set the color you would like to use.', 'ariespolymer' ),
        ) );
        $defaultHeadlineFontSize = array(1 => 30, 2 => 25, 3 => 16, 4 => 16, 5 => 16, 6 => 16);
        foreach(range(1,6) as $h){
            $wp_customize->add_setting('ariespolymer_'.$h.'_font_size', array('default'     => $defaultHeadlineFontSize[$h]));
            $wp_customize->add_control( 'ariespolymer_'.$h.'_font_size', array(
                'label'    => __( 'H'.$h.' Font Size in pixel', 'ariespolymer' ),
                'section'  => 'ariespolymer_headlines_section',
                'settings' => 'ariespolymer_'.$h.'_font_size',
                'type'     => 'select',
                'choices'  => array_combine(range(5,50), range(5,50)),
                'priority'   => 104,
            )  );
        }

    }

}