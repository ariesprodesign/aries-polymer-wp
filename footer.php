<paper-material elevation="1" class="style-scope">
    <section class="footer-media-links style-scope pw-footer social-media-icons">
        <?php Aries_Polymer_Utils::getSocialMediaFooter();?>
    </section>
    <iron-grid class="footer">
        <div class="xl3 l3 m3 s6 xs12">
            <div class="footer-line"></div>
            <paper-material elevation="0" class="style-scope">
                <section id="pages-2" class="widget widget_nav_menu">
                    <?php if ( get_theme_mod( 'ariespolymer_footer_nav_title' ) ) : ?>
                        <h2 class="widget-title footer-links-title">
                            <?php echo get_theme_mod( 'ariespolymer_footer_nav_title' ) ?>
                        </h2>
                    <?php endif; ?>
                    <paper-menu class="footer-links style-scope">
                        <?php if ( has_nav_menu( 'footer' ) ) : ?>
                            <?php
                            wp_nav_menu( array(
                                             'theme_location' => 'footer',
                                             'menu_class'     => 'polymer-footer-menu',
                                             'walker'  => new Aries_Polymer_Walker_Nav_Menu
                                         ) );
                            ?>
                        <?php endif; ?>
                    </paper-menu>
                </section>
            </paper-material>
        </div>
        <div class="xl3 l3 m3 s6 xs12">
            <div class="footer-line"></div>
            <?php if ( is_active_sidebar( 'sidebar-2' )  ) : ?>
                <paper-material elevation="0" class="style-scope"><?php dynamic_sidebar( 'sidebar-2' ); ?></paper-material>
            <?php endif; ?>
        </div>
        <div class="xl3 l3 m3 s6 xs12">
            <div class="footer-line"></div>
            <?php if ( is_active_sidebar( 'sidebar-3' )  ) : ?>
                <paper-material elevation="0" class="style-scope"><?php dynamic_sidebar( 'sidebar-3' ); ?></paper-material>
            <?php endif; ?>
        </div>
        <div class="xl3 l3 m3 s6 xs12">
            <div class="footer-line"></div>
            <?php if ( is_active_sidebar( 'sidebar-4' )  ) : ?>
                <paper-material elevation="0" class="style-scope"><?php dynamic_sidebar( 'sidebar-4' ); ?></paper-material>
            <?php endif; ?>
        </div>
    </iron-grid>
    <div class="copyright layout horizontal style-scope pw-footer">
        <div class="flex style-scope pw-footer"><?php echo Aries_Polymer_Utils::getAriesPolymerThemeMod('ariespolymer_footer_copyright', '©'. date("Y").' Aries Prodesign' )?></div>
    </div>
</paper-material>

</paper-scroll-header-panel>
</paper-drawer-panel>
</template>

<!-- build:js scripts/app.js -->
<script src="<?php echo get_template_directory_uri(); ?>/scripts/app.js"></script>
<!-- endbuild-->
</body>

</html>

