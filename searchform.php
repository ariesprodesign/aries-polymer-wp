
<form is="iron-form" id="search-form" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
    <div class="search-input-wrapper">
        <paper-input name="s" placeholder="<?php echo _x( 'Search for:', 'label', 'ariespolymer' ); ?> " value="<?php echo get_search_query(); ?>" required type="search"></paper-input>
    </div>
    <div class="search-button-wrapper">
        <paper-icon-button  icon="search" onclick="document.getElementById('search-form').submit()"></paper-icon-button>
    </div>
</form>

