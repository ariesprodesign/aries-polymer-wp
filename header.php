<!doctype html>


<html <?php language_attributes(); ?> >

<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="generator" content="Aries Polymer" />
    <!-- Place favicon.ico in the `app/` directory -->

    <!-- Chrome for Android theme color -->
    <meta name="theme-color" content="#303F9F">

    <!-- Tile color for Win8 -->
    <meta name="msapplication-TileColor" content="#3372DF">

    <!-- Add to homescreen for Chrome on Android -->
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="application-name" content="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
    <link rel="icon" sizes="192x192" href="<?php echo Aries_Polymer_Utils::getAriesPolymerThemeMod('header_image', get_template_directory_uri().'/images/icon-128x128.png') ?>">

    <!-- Add to homescreen for Safari on iOS -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
    <link rel="apple-touch-icon" href="<?php echo Aries_Polymer_Utils::getAriesPolymerThemeMod('header_image', get_template_directory_uri().'/images/icon-128x128.png') ?>">

    <!-- endbuild-->

    <!-- build:js bower_components/webcomponentsjs/webcomponents-lite.min.js -->
    <script src="<?php echo get_template_directory_uri(); ?>/bower_components/webcomponentsjs/webcomponents-lite.js"></script>
    <!-- endbuild -->

    <!-- will be replaced with elements/elements.vulcanized.html -->
    <link rel="import" href="<?php echo get_template_directory_uri(); ?>/elements/elements.html">

    <!-- build:css styles/main.css -->
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/styles/main.css">

    <?php Aries_Polymer_Utils::getCustomizeStyleSheets();?>
    <?php wp_head(); ?>
</head>

<body unresolved class="fullbleed layout vertical">
<noscript>JavaScript is off. Please enable to view full site.</noscript>
<span id="browser-sync-binding"></span>
<template is="dom-bind" id="app">

    <paper-drawer-panel id="paperDrawerPanel">
        <!-- Drawer Scroll Header Panel -->
        <paper-scroll-header-panel drawer fixed>

            <!-- Drawer Toolbar -->
            <paper-toolbar id="drawerToolbar">
                <!-- Toolbar icons -->
                <?php get_search_form( true ) ?>
            </paper-toolbar>

            <!-- Drawer Content -->
            <paper-menu class="list">
                <?php if ( has_nav_menu( 'primary' ) ): ?>
                <?php wp_nav_menu( array('theme_location' => 'primary', 'menu_class' => 'polymer-top-menu', 'walker'  => new Aries_Polymer_Walker_Nav_Menu)); ?>
                <?php else: ?>
                    <div class="menu-hauptmenu-container">
                        <ul id="menu-hauptmenu" class="polymer-top-menu">
                            <li id="menu-item-13" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-13">
                                <a href="<?php echo home_url() ?>">
                                    <iron-icon icon="home"></iron-icon><span><?php echo __( 'Home', 'ariespolymer' ) ?></span>
                                </a>
                            </li>
                        </ul>
                    </div>

                <?php endif ?>
            </paper-menu>
            <div class="menu-sidebar">
                <?php get_sidebar()?>
            </div>
        </paper-scroll-header-panel>
        <!-- Main Area -->
        <paper-scroll-header-panel main condenses keep-condensed-header header-height="200" condensed-header-height="60">

            <!-- Main Toolbar -->
            <paper-toolbar id="mainToolbar" class="tall">
                <paper-icon-button id="paperToggle" icon="menu" paper-drawer-toggle></paper-icon-button>
                <span class="flex"></span>
                <?php Aries_Polymer_Utils::getSocialMediaHeader();?>

                <?php if (Aries_Polymer_Utils::getAriesPolymerThemeMod('ariespolymer_logo')) : ?>
                    <div class="middle middle-container center horizontal layout logo-wrapper">
                        <a href='<?php echo esc_url( home_url( '/' ) ); ?>' title='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>' rel='home' class="home-logo app-name">
                        </a>
                    </div>
                    <!-- Application sub title -->
                    <div class="bottom bottom-container center horizontal layout">
                    </div>
                <?php else : ?>
                    <!-- Application name -->
                    <div class="middle middle-container center horizontal layout">
                        <div class="app-name"><?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?></div>
                    </div>

                    <!-- Application sub title -->
                    <div class="bottom bottom-container center horizontal layout">
                        <div class="bottom-title paper-font-subhead"><?php echo esc_attr( get_bloginfo( 'description', 'display' ) ); ?></div>
                    </div>
                <?php endif; ?>

            </paper-toolbar>
