<?php
/*
Template Name: Full-width layout
Template Post Type: page
*/

get_header(); ?>
        <!-- Main Content -->
        <div class="content style-scope">
            <paper-material elevation="0">
                <?php
                if ( have_posts() ) :
                    while ( have_posts() ) : the_post(); ?>
                        <div id="content-wrapper" class="content-wrapper">
                            <h1 class="paper-font-display1 style-scope my-greeting">
                                <span><?php the_title();?></span>
                            </h1>
                            <?php if ( has_post_thumbnail() && ! post_password_required() ) : ?>
                                <div class="article-image">
                                    <iron-image sizing="contain" alt="<?php the_title()?>" src="<?php echo get_the_post_thumbnail_url()?>" style="width:400px; height:400px;" ></iron-image>
                                </div>
                            <?php endif; ?>
                            <?php    the_content(); ?>
                        </div>
                    <?php endwhile; ?>
                <?php endif; ?>

            </paper-material>

        </div>
<?php get_footer();