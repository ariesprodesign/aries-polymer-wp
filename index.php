<?php
$postPageTitle = Aries_Polymer_Utils::getAriesPolymerThemeMod('ariespolymer_post_page_title', 'Post');
get_header();
?>
        <!-- Main Content -->
        <div class="content style-scope">
            <h2 class="paper-font-display1 style-scope my-greeting page-title">
                <span>
                <?php _e( $postPageTitle, 'ariespolymer' ); ?>
                </span>
            </h2>
            <paper-material elevation="0" class="content-two-col">
                <?php if ( is_active_sidebar( 'sidebar-5' )  ) : ?>
                    <div class="sidebar-right-col">
                        <paper-material elevation="0" class="style-scope"><?php dynamic_sidebar( 'sidebar-5' ); ?></paper-material>
                    </div>
                <?php endif; ?>
                <div id="content-wrapper" class="content-left-col content-wrapper">
                    <iron-grid>
                        <?php while (have_posts()) : the_post(); ?>
                            <?php
                            $image = get_template_directory_uri().'/images/default-thumb.jpg';
                            if(has_post_thumbnail() && ! post_password_required()){
                                $image = get_the_post_thumbnail_url();
                            }elseif(get_theme_mod( 'ariespolymer_default_featured_image' ) ){
                                $image = get_theme_mod( 'ariespolymer_default_featured_image' );
                            }

                            ?>
                            <div class="xl4 l4 m4 s6 xs12">
                                <paper-card image="<?php echo $image ?>" heading="<?php echo the_title() ?>"  elevation="1"  class="white post-card">
                                    <div id="post-<?php the_ID(); ?>" class="card-content">
                                        <p class="meta"><?php the_time( get_option( 'date_format' ) ); ?> / <?php the_author(); ?> / <?php the_category(', '); ?></p>
                                        <?php the_excerpt(); ?>
                                    </div>
                                    <div class="card-actions">
                                        <a href="<?php the_permalink() ?>"><paper-button raised><?php _e( 'Read More', 'ariespolymer' ); ?></paper-button></a>
                                    </div>
                                </paper-card>
                            </div>
                        <?php endwhile; ?>
                    </iron-grid>
                    <?php Aries_Polymer_Utils::getPaginationNav() ?>
                </div>
            </paper-material>


        </div>
<?php get_footer();