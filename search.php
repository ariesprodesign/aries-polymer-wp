<?php
get_header(); ?>

<div class="content style-scope">
    <paper-material elevation="0">
        <div id="content-wrapper" class="content-left-col">
        <?php if ( have_posts() ) : ?>

            <header class="page-header">
                <h1 class="page-title"><?php printf( __( 'Search Results for: %s', 'ariespolymer' ), get_search_query() ); ?></h1>
            </header>

            <?php
            while ( have_posts() ) : the_post();
                get_template_part( 'content', 'search' );
            endwhile;
            ?>
            <paper-material elevation="0">
                <?php Aries_Polymer_Utils::getPaginationNav() ?>
            </paper-material>
        <?php else :?>
            <header class="page-header">
                <h1 class="page-title"><?php _e( 'Nothing Found', 'twentyfifteen' ); ?></h1>
            </header>

            <div class="page-content">

                <?php if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>

                    <p><?php printf( __( 'Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'ariespolymer' ), esc_url( admin_url( 'post-new.php' ) ) ); ?></p>

                <?php elseif ( is_search() ) : ?>

                    <p><?php _e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'ariespolymer' ); ?></p>
                    <?php get_search_form(); ?>

                <?php else : ?>

                    <p><?php _e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'ariespolymer' ); ?></p>
                    <?php get_search_form(); ?>

                <?php endif; ?>

            </div>
        <?php  endif; ?>
        </div>
    </paper-material>
</div>

<?php get_footer(); ?>
