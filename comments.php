<?php

if ( post_password_required() ) {
    return;
}
?>

<div id="comments" class="comments-area">

    <?php

    if ( have_comments() ) : ?>
        <h2 class="comments-title">
            <?php
            $comments_number = get_comments_number();
            if ( '1' === $comments_number ) {
                /* translators: %s: post title */
                printf( _x( 'One Reply to &ldquo;%s&rdquo;', 'comments title', 'ariespolymer' ), get_the_title() );
            } else {
                printf(

                    _nx(
                        '%1$s Reply to &ldquo;%2$s&rdquo;',
                        '%1$s Replies to &ldquo;%2$s&rdquo;',
                        $comments_number,
                        'comments title',
                        'ariespolymer'
                    ),
                    number_format_i18n( $comments_number ),
                    get_the_title()
                );
            }
            ?>
        </h2>

        <ol class="comment-list">
            <?php
            wp_list_comments( array(
                                  'walker'            => new Aries_Comments_Walker,
                                  'avatar_size' => 100,
                                  'style'       => 'ol',
                                  'short_ping'  => true,
                                  'reply_text'  => __( 'Reply', 'ariespolymer' ),
                              ) );
            ?>
        </ol>

        <?php the_comments_pagination( array(
                                           'prev_text' => '<span class="screen-reader-text">' . __( 'Previous', 'ariespolymer' ) . '</span>',
                                           'next_text' => '<span class="screen-reader-text">' . __( 'Next', 'ariespolymer' ) . '</span>',
                                       ) );

    endif;

    if ( ! comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) : ?>

        <p class="no-comments"><?php _e( 'Comments are closed.', 'ariespolymer' ); ?></p>
        <?php
    endif;

    comment_form(Aries_Polymer_Utils::getCommentFormArgs());
    ?>

</div>
