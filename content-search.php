<?php

?>

<paper-material elevation="1" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <header class="entry-header">
        <?php if ( 'post' === get_post_type() ) : ?>
            <div class="entry-meta">
                <?php
                echo Aries_Polymer_Utils::ariespolymer_time_link();
                edit_post_link(sprintf( __( 'Edit<span class="screen-reader-text"> "%s"</span>', 'ariespolymer' ), get_the_title()), '<span class="edit-link">', '</span>' );
                ?>
            </div><!-- .entry-meta -->
        <?php elseif ( 'page' === get_post_type() && get_edit_post_link() ) : ?>
            <div class="entry-meta">
                <?php edit_post_link(sprintf( __( 'Edit<span class="screen-reader-text"> "%s"</span>', 'ariespolymer' ), get_the_title()), '<span class="edit-link">', '</span>' ); ?>
            </div><!-- .entry-meta -->
        <?php endif; ?>

        <?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
    </header><!-- .entry-header -->

    <div class="entry-summary">
        <?php the_excerpt(); ?>
    </div><!-- .entry-summary -->

</paper-material><!-- #post-## -->
