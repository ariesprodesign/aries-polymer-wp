<?php
/*
Template Name: Full-width layout
Template Post Type: post
*/

get_header(); ?>
        <!-- Main Content -->
        <div class="content style-scope">
            <paper-material elevation="0">
                <?php while (have_posts()) : the_post(); ?>
                    <div id="content-wrapper" class="content-wrapper">
                    <h1 class="paper-font-display1 style-scope my-greeting">
                        <span><?php the_title();?></span>
                    </h1>
                    <article class="article-single">
                        <?php if ( has_post_thumbnail() && ! post_password_required() ) : ?>
                            <div class="article-image">
                                <iron-image sizing="contain" alt="<?php the_title()?>" src="<?php echo get_the_post_thumbnail_url()?>" style="width:400px; height:400px;" ></iron-image>
                            </div>
                        <?php endif; ?>
                        <p class="meta"><?php the_time( get_option( 'date_format' ) ); ?> / <?php the_author(); ?> / <?php the_category(', '); ?></p>
                        <?php the_content(); ?>
                        <div class="padinate-page"><?php wp_link_pages(); ?></div>
                        <p class="meta_tags"><?php the_tags(); ?></p>
                        <?php comments_template(); ?>
                    </article>
                </div>
                <?php endwhile; ?>

            </paper-material>

        </div>
<?php get_footer();