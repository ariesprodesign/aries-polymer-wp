=== Aries Polymer WP ===
Author: Aries Prodesign
Requires at least: WordPress 4.7
Tested up to: WordPress 4.7
Version: 1.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Tags: polymer, responsive, left-sidebar, flexible-header, custom-colors, custom-header, custom-menu, custom-logo, editor-style, featured-images, footer-widgets, post-formats, rtl-language-support, sticky-post, theme-options, threaded-comments, translation-ready

== Description ==

Aries Polymer WP has been translated into the following languages:
Englisch
Deutsch
Spanisch

This WordPress Theme is in active development and will be updated on a regular basis - Please do not rate negative before we tried to solve your issue.

For more information about Aries Polymer WP please go to http://aries-polymer.ariesprodesign.de

== Installation ==

1. In your admin panel, go to Appearance -> Themes and click the 'Add New' button.
2. Type in Aries Polymer WP in the search form and press the 'Enter' key on your keyboard.
3. Click on the 'Activate' button to use your new theme right away.
4. Go to http://aries-polymer.ariesprodesign.de for a guide on how to customize this theme.
5. Navigate to Appearance > Customize in your admin panel and customize to taste.


Aries Polymer WP bundles the following third-party resources:

Polymer Project, Copyright Polymer Authors
Code Licensed under the BSD License
Documentation licensed under CC BY 3.0.
Source: https://www.polymer-project.org/1.0/

== Changelog ==

= 1.0 =
* Released: December 12, 2016

Initial release


=============================================================================================================
Aries Polymer WP use the following polymer elements (These elements you can also use them in the WordPress editor. You can use the WordPress editor with the Polymer tags directly):

Iron elements

iron-flex-layout
iron-a11y-announcer
iron-a11y-keys
iron-ajax
iron-autogrow-textarea
iron-doc-viewer
iron-dropdown
iron-jsonp-library
iron-list
iron-localstorage
iron-media-query
iron-pages
iron-swipeable-container
iron-icons
iron-image
iron-pages
iron-selector
iron-grid
iron-collapse
iron-form
Paper elements

paper-drawer-panel
paper-icon-button
paper-item
paper-material
paper-card
paper-menu
paper-icon-item
paper-scroll-header-panel
paper-styles-classes
paper-toast
paper-toolbar
paper-button
paper-input
paper-textarea
paper-dialog
paper-badge
paper-checkbox
paper-dialog-scrollable
paper-dropdown-menu
paper-fab
paper-listbox
paper-menu-button
paper-progress
paper-radio-button
paper-radio-group
paper-ripple
paper-slider
paper-spinner
paper-tabs
paper-toggle-button
paper-tooltip
Other elements

sc-swiper
social-media-icons
google-youtube
