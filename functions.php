<?php

require_once(get_template_directory()."/classes/Aries_Polymer_Setup.php");
require_once(get_template_directory()."/classes/Aries_Polymer_Walker_Nav_Menu.php");
require_once(get_template_directory()."/classes/Aries_Comments_Walker.php");
require_once(get_template_directory()."/classes/Aries_Polymer_Utils.php");
require_once(get_template_directory()."/classes/Aries_Polymer_Shortcodes.php");
require_once(get_template_directory()."/classes/Aries_Polymer_Nav_Menu_Icons.php");

$aries_polymer_setup = new Aries_Polymer_Setup();

add_action('get_header', 'remove_admin_login_header');
function remove_admin_login_header() {
    remove_action('wp_head', '_admin_bar_bump_cb');
}
add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
    add_theme_support( 'woocommerce' );
}






