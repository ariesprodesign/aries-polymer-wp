<?php get_header(); ?>
        <!-- Main Content -->
        <div class="content style-scope">
            <paper-material elevation="0" class="content-two-col">
                <div class="sidebar-right-col">
                    <paper-material elevation="0" class="style-scope"><?php dynamic_sidebar( 'sidebar-5' ); ?></paper-material>
                </div>

                <div id="content-wrapper" class="content-left-col content-wrapper">
                    <?php woocommerce_content();?>
                </div>
            </paper-material>

        </div>
<?php get_footer();